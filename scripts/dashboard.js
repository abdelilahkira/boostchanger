// All of the Node.js APIs are available in the preload process.
// It has the same sandbox as a Chrome extension.
const { exec } = require("child_process");
const os = require('os');

window.addEventListener('DOMContentLoaded', () => {
  // close app if user hit close button
  const closeApp = document.getElementById('close');
  closeApp.addEventListener('click', () => {
    window.close();
  });
  // get cpu name
  document.getElementById("cpu_name").innerHTML = os.cpus()[0].model
  // get total memory
  var total_memory = (os.totalmem() / 1024 / 1024 / 1024)
  // toFixed function is for round the number (Math)
  document.getElementById("mem_total").innerHTML = total_memory.toFixed(0) + " GB";
  // exec function to get the free memory for the first time
  exec("cat /proc/meminfo | grep 'MemAvailable' | awk '{ print $2 }'", (stderr, stdout) => {
    if (stderr instanceof Error) { throw stderr; }
    var free_memory = stdout / 1024 / 1024
    document.getElementById("free_mem").innerHTML = free_memory.toFixed(2) + " GB"
  })
  // function for the interval to update free mem every 25 seconds
  function freeMem() {
    setInterval(() => {
      exec("cat /proc/meminfo | grep 'MemAvailable' | awk '{ print $2 }'", (stderr, stdout) => {
        if (stderr instanceof Error) { throw stderr; }
        var intervalFreeMem = stdout / 1024 / 1024
        document.getElementById("free_mem").innerHTML = intervalFreeMem.toFixed(2) + " GB"
      })
    }, 25000) //25000 millisecond = 25 seconds
  }
  // call freeMem
  freeMem();

  // IP
  const ip = require("ip")
  document.getElementById("local_ip").innerHTML = ip.address();

  // Public IP 
  const publicIp = require('public-ip');
  (async () => {
    document.getElementById("public_ip").innerHTML = (await publicIp.v4())
  })();

  // Kernel
  exec("uname -r", (stderr, stdout) => {
    if (stderr instanceof Error) { throw stderr; }
    document.getElementById("kernel").innerHTML = stdout
  })

  // OS Name
  exec("lsb_release -d | awk '{ print $2,$3}'", (stderr, stdout) => {
    if (stderr instanceof Error) { throw stderr; }
    document.getElementById("os_name").innerHTML = stdout
  })

  // Up time 
  var up_time = os.uptime();
  var intervalUpTime = up_time / 3600
  document.getElementById("up_time").innerHTML = intervalUpTime.toFixed(2) + " hours"

})
