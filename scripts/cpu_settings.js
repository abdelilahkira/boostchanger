const sudo = require('sudo-prompt');
var fs = require('fs');
const readline = require('readline');
const os = require('os');

window.addEventListener('DOMContentLoaded', () => {
  // close app if user hit close button
  const closeApp = document.getElementById('close');
  closeApp.addEventListener('click', () => {
    window.close();
  })
  //sudo-prompt needs always options with the name
  var options = {
    name: 'Boost Changer'
  };
  var no_turbo = readline.createInterface({
    input: fs.createReadStream('/sys/devices/system/cpu/intel_pstate/no_turbo')
  });
  // function checked which state has no_turbo 0 or 1 when user starts this app. 
  no_turbo.on('line', (line) => {
    if (line == 0) {
      document.getElementById("turbo_boost_status").innerHTML = "ON";
      document.getElementById("turbo_toggle").checked = false;
    } else {
      document.getElementById("turbo_boost_status").innerHTML = "OFF";
      document.getElementById("turbo_toggle").checked = true;
    }
    document.getElementById("turbo_toggle").addEventListener('change', () => {

      var turbo_toggle = document.getElementById("turbo_toggle");
      if (turbo_toggle.checked == true) {
        sudo.exec("echo 1 > /sys/devices/system/cpu/intel_pstate/no_turbo", options, (stderr) => {
          if (stderr instanceof Error) { throw stderr; }
          new Notification('Boost Changer', { body: 'Turbo Boost is now OFF' })
          document.getElementById("turbo_boost_status").innerHTML = "OFF";
        })
      } else {
        sudo.exec("echo 0 > /sys/devices/system/cpu/intel_pstate/no_turbo", options, (stderr) => {
          if (stderr instanceof Error) { throw stderr }
          new Notification('Boost Changer', { body: 'Turbo Boost is now ON' })
          document.getElementById("turbo_boost_status").innerHTML = "ON";
        })
      }
    })
  })
  function cpuInfo() {
    // Get CPU speed for the first time
    document.getElementById('cpu_MHz').innerHTML = os.cpus()[0].speed + " MHz";
    setInterval(() => {
      document.getElementById('cpu_MHz').innerHTML = os.cpus()[0].speed + " MHz";
    }, 1000); //setintervall for 1 sec
  }
  cpuInfo();
  var badgeTag = document.getElementById('bdg');
  var max_perf = readline.createInterface({
    input: fs.createReadStream('/sys/devices/system/cpu/intel_pstate/max_perf_pct')
  });
  // function checked which state has no_turbo 0 or 1 when user starts this app. 
  max_perf.on('line', (line) => {
    if (line == 30) {
      badgeTag.classList.add("badge-primary");
      badgeTag.innerHTML = "Power Save";
    } else if (line == 50) {
      badgeTag.classList.add("badge-primary");
      badgeTag.innerHTML = "Balance";
    } else if (line == 70) {
      badgeTag.classList.add("badge-primary");
      badgeTag.innerHTML = "Performance";
    } else {
      badgeTag.classList.add("badge-primary");
      badgeTag.innerHTML = "Ultra";
    }
  })
  document.getElementById('btn-save').addEventListener('click', () => {
    sudo.exec("echo 30 > /sys/devices/system/cpu/intel_pstate/max_perf_pct", options, (stderr) => {
      if (stderr instanceof Error) { throw stderr; }
      badgeTag.classList.add("badge-primary");
      badgeTag.innerHTML = "Power Save";
    })
  });
  document.getElementById('btn-balance').addEventListener('click', () => {
    sudo.exec("echo 50 > /sys/devices/system/cpu/intel_pstate/max_perf_pct", options, (stderr) => {
      if (stderr instanceof Error) { throw stderr; }
      badgeTag.classList.add("badge-primary");
      badgeTag.innerHTML = "Balance";
    })
  });
  document.getElementById('btn-perf').addEventListener('click', () => {
    sudo.exec("echo 70 > /sys/devices/system/cpu/intel_pstate/max_perf_pct", options, (stderr) => {
      if (stderr instanceof Error) { throw stderr; }
      badgeTag.classList.add("badge-primary");
      badgeTag.innerHTML = "Performance";
    })
  });
  document.getElementById('btn-ultra').addEventListener('click', () => {
    sudo.exec("echo 100 > /sys/devices/system/cpu/intel_pstate/max_perf_pct", options, (stderr) => {
      if (stderr instanceof Error) { throw stderr; }
      badgeTag.classList.add("badge-primary");
      badgeTag.innerHTML = "Ultra";
    })
  });
})