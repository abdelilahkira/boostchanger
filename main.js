// Modules to control application life and create native browser window
const { app, BrowserWindow, Menu } = require('electron')
const path = require('path')

// Definition for new window (Error)
var errorWindow = null
function openErrorWindow() {
  errorWindow = new BrowserWindow({
    height: 160,
    resizable: false,
    width: 300,
    title: 'Error',
    minimizable: false,
    icon: path.join(__dirname, 'icon/boostChanger.png')
  })
  // About window
  errorWindow.loadFile(path.join(__dirname, 'error.html'))
  //Garbage collection handle
  errorWindow.on('closed', function () {
    errorWindow = null
  })
  errorWindow.setMenu(null)
}

function createWindow() {

  // Create the main window.
  const mainWindow = new BrowserWindow({
    width: 1000,
    height: 700,
    resizable: false, //TODO
    title: 'Boost Changer v3.4',
    webPreferences: {
      //preload: path.join(__dirname, 'preload.js'),
      nodeIntegration: true,
    },
    icon: path.join(__dirname, 'icon/boostChanger.png')
  })

  // and load the index.html of the app.
  mainWindow.loadFile('index.html')

  // Open the DevTools.
  //mainWindow.webContents.openDevTools() 
  //the entire App will close
  mainWindow.on('close', () => { app.quit(); });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
  // //TODO This code for DEV
  // createWindow();
  // app.on('activate', function () {
  //   // On macOS it's common to re-create a window in the app when the
  //   // dock icon is clicked and there are no other windows open.
  //   if (BrowserWindow.getAllWindows().length === 0) createWindow()
  // })//TODO:TILL HERE

  const fs = require('fs')
  //show error when the user uses VM 

  if (fs.existsSync('/sys/devices/system/cpu/intel_pstate/no_turbo')) {
    createWindow()

    app.on('activate', function () {
      // On macOS it's common to re-create a window in the app when the
      // dock icon is clicked and there are no other windows open.
      if (BrowserWindow.getAllWindows().length === 0) createWindow()
    })
  } else {
    openErrorWindow()
    app.on('activate', function () {
      // On macOS it's common to re-create a window in the app when the
      // dock icon is clicked and there are no other windows open.
      if (BrowserWindow.getAllWindows().length === 0) openErrorWindow()
    })
  }

})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
const isMac = process.platform === "darwin";
const template = [
  {
    label: "File",
    submenu: [isMac ? { role: "close" } : { role: "quit" }],
  },
  { label: '|' },
  {
    label: 'Help',
    click() {
      openHelpWindow()
    }
  },
];

// Definition for new window (About)
var newWindow = null
function openHelpWindow() {
  if (newWindow) {
    newWindow.focus()
    return
  }
  newWindow = new BrowserWindow({
    height: 250,
    resizable: false,
    width: 450,
    title: 'Help',
    minimizable: false,
    icon: path.join(__dirname, 'icon/boostChanger.png')
  })
  // About window
  newWindow.loadFile(path.join(__dirname, 'help.html'))
  // Open the DevTools
  //newWindow.webContents.openDevTools()

  //Garbage collection handle
  newWindow.on('closed', function () {
    newWindow = null
  })
  newWindow.setMenu(null)
  //open external link from a normal browser only in about.html
  newWindow.webContents.on("new-window", (e, url) => {
    e.preventDefault();
    require("electron").shell.openExternal(url);
  })
}

const menu = Menu.buildFromTemplate(template);
Menu.setApplicationMenu(menu);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});