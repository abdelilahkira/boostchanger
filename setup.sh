#!/bin/bash

pkgver=3.4
pkgverOLD=3.2

if [[ -d /opt/boostchanger-v$pkgverOLD ]]
then
    rm rf /opt/boostchanger-v$pkgverOLD
    cd app/
    cp -r boostchanger-v$pkgver /opt
    install -Dm755 boostchanger.sh /usr/bin/boostchanger
    install -Dm644 boostchanger.desktop /usr/share/applications/boostchanger.desktop
    install -Dm644 boostchanger.png /usr/share/pixmaps/boostchanger.png
else
    cd app/
    cp -r boostchanger-v$pkgver /opt
    install -Dm755 boostchanger.sh /usr/bin/boostchanger
    install -Dm644 boostchanger.desktop /usr/share/applications/boostchanger.desktop
    install -Dm644 boostchanger.png /usr/share/pixmaps/boostchanger.png
fi 

